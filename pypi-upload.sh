#!/bin/bash

rm -rf dist UQToolbox.egg UQToolbox.egg-info
python setup.py sdist bdist_wheel
twine upload dist/*

