.. DABI_UQToolbox documentation master file, created by
   sphinx-quickstart on Fri Jan 18 16:48:30 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to DABI_UQToolbox's documentation!
==========================================

Contents:

.. toctree::
   :maxdepth: 2

.. automodule:: UQToolbox

	.. automodule:: UQToolbox.RandomSampling
		:members:

	.. automodule:: UQToolbox.gPC
		:members:

	.. automodule:: UQToolbox.CutANOVA
		:members:
		
	.. automodule:: UQToolbox.mcmc
		:members:

	.. automodule:: UQToolbox.ModelReduction
		:members:

	.. automodule:: UQToolbox.sobol_lib
		:members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

